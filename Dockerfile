FROM golang:1.16-alpine AS builder

WORKDIR /build_dir
ADD ./ ./

RUN apk --update --no-cache add make git
RUN make bin

WORKDIR /
ENV GOBIN /usr/local/bin
RUN go install github.com/go-delve/delve/cmd/dlv@latest

FROM alpine:latest

COPY --from=builder /usr/local/bin/dlv /usr/local/bin/dlv
COPY --from=builder /build_dir/api-server /usr/local/bin/api-server
COPY --from=builder /build_dir/config/config.yml /etc/api-server/config.yaml

CMD ["dlv", "--listen=:2345", "--headless=true", "--api-version=2", "--accept-multiclient", "exec", "/usr/local/bin/api-server", "config", "/etc/api-server/config.yaml"]
module gitlab.com/Zeloid/keeper-core

go 1.16

require (
	github.com/alecthomas/kong v0.2.17
	github.com/go-masonry/bjaeger v1.0.9
	github.com/go-masonry/bprometheus v1.0.9
	github.com/go-masonry/bviper v1.0.9
	github.com/go-masonry/bzerolog v1.0.9
	github.com/go-masonry/mortar v1.0.15
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.5.0
	github.com/opentracing/opentracing-go v1.2.0
	go.uber.org/fx v1.14.0
	google.golang.org/genproto v0.0.0-20210813162853-db860fec028c
	google.golang.org/grpc v1.40.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0
	google.golang.org/protobuf v1.27.1
)

package controllers

import (
	"context"

	core "gitlab.com/Zeloid/keeper-core/api"

	"github.com/go-masonry/mortar/interfaces/log"
	"go.uber.org/fx"
)

type CoreController interface {
	core.CoreServer
}

type coreControllerDeps struct {
	fx.In

	Logger log.Logger
}

type coreControllerImpl struct {
	*core.UnimplementedCoreServer // if keep this one added even when you change your interface this code will compile
	deps                          coreControllerDeps
}

func CreateCoreController(deps coreControllerDeps) CoreController {
	return &coreControllerImpl{
		deps: deps,
	}
}

func (w *coreControllerImpl) Initialize(ctx context.Context, req *core.InitializeRequest) (*core.InitializeReply, error) {
	w.deps.Logger.Debug(ctx, "initialization requested", req.GetTimezone())
	return &core.InitializeReply{
		Message: "Accepted " + req.GetTimezone(),
	}, nil
}

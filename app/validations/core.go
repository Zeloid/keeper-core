package validations

import (
	"context"
	"fmt"

	"github.com/go-masonry/mortar/interfaces/auth/jwt"
	"github.com/go-masonry/mortar/interfaces/log"
	core "gitlab.com/Zeloid/keeper-core/api"
	"go.uber.org/fx"
)

type CoreValidations interface {
	core.CoreServer
}

type coreValidationsDeps struct {
	fx.In

	jwt.TokenExtractor
	Logger log.Logger
}

type coreValidationsImpl struct {
	*core.UnimplementedCoreServer
	deps coreValidationsDeps
}

func CreateCoreValidations(deps coreValidationsDeps) CoreValidations {
	return &coreValidationsImpl{
		deps: deps,
	}
}

func (impl *coreValidationsImpl) Initialize(ctx context.Context, req *core.InitializeRequest) (_ *core.InitializeReply, err error) {
	if err = impl.CheckAuth(ctx); err != nil {
		return nil, err
	}
	if len(req.GetTimezone()) == 0 {
		return nil, fmt.Errorf("timezone cannot be empty")
	}
	return nil, nil
}

package services

import (
	"context"

	"github.com/go-masonry/mortar/interfaces/monitor"
	core "gitlab.com/Zeloid/keeper-core/api"
	"gitlab.com/Zeloid/keeper-core/app/controllers"
	"gitlab.com/Zeloid/keeper-core/app/validations"

	"github.com/go-masonry/mortar/interfaces/log"
	"go.uber.org/fx"
)

type coreServiceDeps struct {
	fx.In

	Logger log.Logger

	Validations validations.CoreValidations
	Controller  controllers.CoreController
	Metrics     monitor.Metrics `optional:"true"`
}

type coreServiceImpl struct {
	core.UnimplementedCoreServer // if keep this one added even when you change your interface this code will compile
	deps                         coreServiceDeps
}

func CreateCoreService(deps coreServiceDeps) core.CoreServer {
	return &coreServiceImpl{
		deps: deps,
	}
}

func (impl *coreServiceImpl) Initialize(ctx context.Context, req *core.InitializeRequest) (*core.InitializeReply, error) {
	_, err := impl.deps.Validations.Initialize(ctx, req)
	if err != nil {
		impl.deps.Logger.WithError(err).Debug(ctx, "validation failed")
		return nil, err
	}
	return impl.deps.Controller.Initialize(ctx, req)
}

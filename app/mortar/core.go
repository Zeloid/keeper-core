package mortar

import (
	"context"

	serverInt "github.com/go-masonry/mortar/interfaces/http/server"
	"github.com/go-masonry/mortar/providers/groups"
	core "gitlab.com/Zeloid/keeper-core/api"
	"gitlab.com/Zeloid/keeper-core/app/controllers"
	"gitlab.com/Zeloid/keeper-core/app/services"
	"gitlab.com/Zeloid/keeper-core/app/validations"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"go.uber.org/fx"
	"google.golang.org/grpc"
)

type workshopServiceDeps struct {
	fx.In

	// API Implementations, "Register" them as GRPCServiceAPI
	Core core.CoreServer
}

func ServiceAPIsAndOtherDependenciesFxOption() fx.Option {
	return fx.Options(
		// GRPC Service APIs registration
		fx.Provide(fx.Annotated{
			Group:  groups.GRPCServerAPIs,
			Target: serviceGRPCServiceAPIs,
		}),
		// GRPC Gateway Generated Handlers registration
		fx.Provide(fx.Annotated{
			Group:  groups.GRPCGatewayGeneratedHandlers + ",flatten", // "flatten" does this [][]serverInt.GRPCGatewayGeneratedHandlers -> []serverInt.GRPCGatewayGeneratedHandlers
			Target: serviceGRPCGatewayHandlers,
		}),
		// All other tutorial dependencies
		serviceDependencies(),
	)
}

func serviceGRPCServiceAPIs(deps workshopServiceDeps) serverInt.GRPCServerAPI {
	return func(srv *grpc.Server) {
		core.RegisterCoreServer(srv, deps.Core)
		// Any additional gRPC Implementations should be called here
	}
}

func serviceGRPCGatewayHandlers() []serverInt.GRPCGatewayGeneratedHandlers {
	return []serverInt.GRPCGatewayGeneratedHandlers{
		// Register service REST API
		func(mux *runtime.ServeMux, localhostEndpoint string) error {
			return core.RegisterCoreHandlerFromEndpoint(context.Background(), mux, localhostEndpoint, []grpc.DialOption{grpc.WithInsecure()})
		},
		// Any additional gRPC gateway registrations should be called here
	}
}

func serviceDependencies() fx.Option {
	return fx.Provide(
		services.CreateCoreService,
		controllers.CreateCoreController,
		validations.CreateCoreValidations,
	)
}
